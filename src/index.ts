
#!/usr/bin/env ts-node
/* eslint-disable import/no-extraneous-dependencies */

import { exec as execCallback } from "child_process";
import * as fs from "fs/promises";
import * as path from "path";
import { promisify } from "util";
import Axios, { AxiosInstance } from "axios";
import { program } from "commander";
import Listr from "listr";

const packageContents = require("./package.json");

const exec = promisify(execCallback);

const getApi = (
  isPrivate: boolean,
  { gitlabUrl, privateToken, token }: JobContext
): AxiosInstance => {
  const headers = {} as Record<string, string>;

  if (isPrivate) {
    headers["PRIVATE-TOKEN"] = privateToken;
  } else {
    headers["JOB-TOKEN"] = token;
  }

  return Axios.create({
    baseURL: gitlabUrl,
    headers,
  });
};

async function getLatestVersion() {
  const versionResult = await exec(
    `git tag --list --sort=-version:refname "v*" | head -n 1`
  );

  const version = versionResult.stdout.trim().replace("v", "");
  console.info(`Version ${version}`);

  return version;
}

async function createGitLabRelease(context: JobContext) {
  const { newVersion: version, projectId } = context;
  console.info(`Creating GitLab Release`);
  const changelog = await fs.readFile(require.resolve("./CHANGELOG.md"), {
    encoding: "utf-8",
  });

  const releaseData = {
    name: `Version ${version}`,
    tag_name: `v${version}`,
    description: changelog,
    ref: "main",
  };

  await getApi(false, context).post(
    `/projects/${projectId}/releases`,
    releaseData
  );
}

const publishToNpm = (context: JobContext): Listr => {
  const { projectId, gitlabUrl, rootNamespace, token, newVersion } = context;

  return new Listr([
    {
      title: `Create .npmrc`,
      async task() {
        await fs.writeFile(
          path.join(__dirname, ".npmrc"),
          [
            `@${rootNamespace}:registry=${gitlabUrl}/projects/${projectId}/packages/npm/`,
            `${gitlabUrl.replace(
              "https:",
              ""
            )}/projects/${projectId}/packages/npm/:_authToken=${token}`,
          ].join("\n")
        );
      },
    },
    {
      title: `Publish Package`,
      task: () => exec(`npm publish`),
    },
    {
      title: `Update Package Badge`,
      async task() {
        const api = getApi(true, context);

        const badges = await api.get<Array<{ name: string; id: number }>>(
          `/projects/${projectId}/badges`
        );
        const versionBadge = badges.data.find(
          (badge) => badge.name === "version"
        );

        if (!versionBadge) {
          throw new Error(`Version badge not found!`);
        }

        await api.put(
          `/projects/${projectId}/badges/${versionBadge!.id}`,
          `image_url=https://img.shields.io/badge/%F0%9F%93%A6%20Version-${newVersion}-blue`
        );
      },
    },
  ]);
};

program
  .option("--token <token>", "GitLab Auth Token", process.env.CI_JOB_TOKEN)
  .option(
    "--privateToken <privateToken>",
    "Access token used to change badge images",
    process.env.GITLAB_CD_TOKEN
  )
  .option(
    "--gitlab-url <gitlabUrl>",
    "URL of GitLab API",
    process.env.CI_API_V4_URL
  )
  .option(
    "--project-id <projectId>",
    "GitLab Project ID",
    process.env.CI_PROJECT_ID
  )
  .option(
    "--root-namespace <rootNamespace>",
    "GitLab Root Namespace",
    process.env.CI_PROJECT_ROOT_NAMESPACE
  );

program.parse(process.argv);

interface JobContext {
  token: string;
  privateToken: string;
  gitlabUrl: string;
  projectId: string;
  rootNamespace: string;
  oldVersion: string;
  newVersion: string;
}

const tasks = new Listr<JobContext>([
  {
    title: `Get Current Version`,
    task: async (context) => {
      context.oldVersion = await getLatestVersion();

      await fs.writeFile(
        require.resolve("./package.json"),
        JSON.stringify(
          {
            ...packageContents,
            version: context.oldVersion,
          },
          null,
          2
        )
      );
    },
  },
  {
    title: `Create New Version`,
    task: async (context) => {
      await exec(`./node_modules/.bin/standard-version`);
      context.newVersion = await getLatestVersion();
    },
  },
  {
    title: `Publish`,
    skip: ({ oldVersion, newVersion }) => oldVersion === newVersion,
    task: (context) =>
      new Listr(
        [
          {
            title: `NPM Package`,
            task: () => publishToNpm(context),
          },
          {
            title: `GitLab Release`,
            task: () => createGitLabRelease(context),
          },
        ],
        { exitOnError: true }
      ),
  },
]);

tasks.run(program.opts<JobContext>()).catch((err) => console.error(err));
